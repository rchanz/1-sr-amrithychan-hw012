import React from "react";
import "./App.css";
import Navbar from "./components/Navbar";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Index from "./components/Index";
import Home from "./components/Home";
import Video from "./components/Video";
import Account from "./components/Account";
import InputForm from "./components/Auth/InputForm";
import Post from "./components/Post";
import ProtectedRoute from "./components/Auth/ProtectedRoute";
import Welcome from "./components/Auth/Welcome";
import Error from "./components/Error";
function App() {
  return (
    <div>
      <Router>
        <Navbar />
        <main className="container">
          <Switch>
            <Route path="/" exact component={Index} />
            <Route path="/home" exact component={Home} />
            <Route path="/video" exact component={Video} />
            <Route path="/account" exact component={Account} />
            <Route path="/auth" exact component={InputForm} />
            <Route path="/post/:id" exact component={Post} />
            <ProtectedRoute exact path="/welcome" component={Welcome} />
            <Route path="*" component={Error} />
          </Switch>
        </main>
      </Router>
    </div>
  );
}

export default App;
