import React from "react";
import Movie from "./Video/Movie";
import Animation from "./Video/Animation";
import { Link, Route, Switch, useRouteMatch } from "react-router-dom";

function Video() {
  let { path, url } = useRouteMatch();

  return (
    <div className="mt-3">
      <ul>
        <li>
          <Link to={`${url}/movie`}>Movie</Link>
        </li>
        <li>
          <Link to={`${url}/animation`}>Animation</Link>
        </li>
      </ul>

      <Switch>
        <Route exact path={path}>
          <h3>Please select a topic!</h3>
        </Route>
        <Route path={`${path}/movie`}>
          <Movie />
        </Route>
        <Route path={`${path}/animation`}>
          <Animation />
        </Route>
      </Switch>
    </div>
  );
}

export default Video;
