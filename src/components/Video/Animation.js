import React from "react";
import {
  Link,
  Switch,
  Route,
  useRouteMatch,
  useParams,
} from "react-router-dom";

function Animation() {
  let { path, url } = useRouteMatch();

  return (
    <div>
      <h2>Animation Category</h2>
      <ul>
        <li>
          <Link to={`${url}/action`}>Action</Link>
        </li>
        <li>
          <Link to={`${url}/romance`}>Romance</Link>
        </li>
        <li>
          <Link to={`${url}/comedy`}>Comedy</Link>
        </li>
      </ul>

      <Switch>
        <Route exact path={path}>
          <h3>Please select a topic.</h3>
        </Route>
        <Route path={`${path}/:topicId`}>
          <Topic />
        </Route>
      </Switch>
    </div>
  );
}

function Topic() {
  let { topicId } = useParams();

  return (
    <div>
      <h3>{topicId}</h3>
    </div>
  );
}

export default Animation;
