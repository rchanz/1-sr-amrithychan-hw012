import React from "react";
import {
  Link,
  Switch,
  Route,
  useRouteMatch,
  useParams,
} from "react-router-dom";

function Movie() {
  let { path, url } = useRouteMatch();

  return (
    <div>
      <h2>Movie Category</h2>
      <ul>
        <li>
          <Link to={`${url}/action`}>Action</Link>
        </li>
        <li>
          <Link to={`${url}/romance`}>Romance</Link>
        </li>
        <li>
          <Link to={`${url}/comedy`}>Comedy</Link>
        </li>
      </ul>

      <Switch>
        <Route exact path={path}>
          <h3>Please select a topic.</h3>
        </Route>
        <Route path={`${path}/:topicId`}>
          <Topic />
        </Route>
      </Switch>
    </div>
  );
}

function Topic() {
  // The <Route> that rendered this component has a
  // path of `/topics/:topicId`. The `:topicId` portion
  // of the URL indicates a placeholder that we can
  // get from `useParams()`.
  let { topicId } = useParams();

  return (
    <div>
      <h3>{topicId}</h3>
    </div>
  );
}

export default Movie;
