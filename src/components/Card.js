import React from "react";
import { Link } from "react-router-dom";

function Card({ id }) {
  const cardStyle = {
    height: "680px",
  };
  return (
    <div className="col-sm-4">
      <div className="card m-1" style={cardStyle}>
        <img
          src="https://i2.wp.com/www.bestanimemerch.com/wp-content/uploads/2019/05/SoloStatue.jpg?fit=540%2C658&ssl=1"
          className="card-img-top img-fluid"
          alt="min-byung-goo"
        />
        <div className="card-body">
          <h5 className="card-title">Card title</h5>
          <p className="card-text">
            This is a wider card with supporting text below as a natural lead-in
            to additional content. This content is a little bit longer.
          </p>
          <Link to={"/post/" + id}>See more</Link>
        </div>
        <div className="card-footer">
          <small className="text-muted">Last updated 3 mins ago</small>
        </div>
      </div>
    </div>
  );
}

export default Card;
