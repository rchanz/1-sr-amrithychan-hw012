import React from "react";
import { Route, Redirect } from "react-router-dom";
import Auth from "./Auth";

function ProtectedRoute({ component: Component, ...rest }) {
  return (
    <div>
      <Route
        {...rest}
        render={(props) => {
          if (Auth.isAuth()) {
            return <Component {...props} />;
          } else {
            return (
              <Redirect
                to={{
                  pathname: "/auth",
                  state: {
                    from: props.location,
                  },
                }}
              />
            );
          }
        }}
      />
    </div>
  );
}

export default ProtectedRoute;
