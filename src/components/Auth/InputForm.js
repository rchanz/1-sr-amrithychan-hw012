import React from "react";
import Auth from "./Auth";

function InputForm(props) {
  return (
    <div className="mt-3">
      <input type="text" placeholder="input username" />
      <input type="password" placeholder="input password" />
      <button
        onClick={() => {
          Auth.login(() => {
            props.history.push("/welcome");
          });
        }}
      >
        Login
      </button>
    </div>
  );
}

export default InputForm;
