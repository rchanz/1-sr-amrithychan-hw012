import React from "react";
import { useParams } from "react-router";

function Post() {
  let { id } = useParams();
  return (
    <div>
      <h3>Currently on post no. {id}</h3>
    </div>
  );
}

export default Post;
