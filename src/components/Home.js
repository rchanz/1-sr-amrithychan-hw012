import React from "react";
import Card from "./Card";

function Home() {
  return (
    <div className="row">
      <Card id={1} />
      <Card id={2} />
      <Card id={3} />
      <Card id={4} />
      <Card id={5} />
      <Card id={6} />
    </div>
  );
}

export default Home;
